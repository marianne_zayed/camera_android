package com.linkdev.camera.interfaces;

/**
 * Created by Marianne.Wazif on 19-Feb-17.
 */

public interface PermissionsInterfaceListener {
    void onWriteExternalStorageGranted();

    void onWriteExternalStorageDenied();

    void onCameraAccessGranted();

    void onCameraAccessDenied();

    void onCameraAccessAndWriteExternalStorageDenied();

    void onCameraAccessAndWriteExternalStorageGranted();

}
