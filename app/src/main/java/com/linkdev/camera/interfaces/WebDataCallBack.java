package com.linkdev.camera.interfaces;

import org.json.JSONException;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by Marianne.Wazif on 22-May-17.
 */

public interface WebDataCallBack {

    /**
     * back in the ui thread, update it
     * @param e
     */
    public void resumeUi(Object result);

    /**
     * handle exception in UI thread
     * @param e
     */
    public void handleException(Exception e);

    /**
     * call the service here, in a separate thread
     * @throws IOException
     * @throws URISyntaxException
     */
    public Object callService() throws IOException, JSONException, URISyntaxException;


}
