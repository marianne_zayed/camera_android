package com.linkdev.camera.helpers;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.os.Build;

/**
 * Created by Marianne.Wazif on 10-Oct-17.
 */

public class CameraManagerHelper {
    private static final float LEFT_POSITION = 0f;
    private static final float TOP_POSITION = 0f;
    private static final int X_POSITION = 0;
    private static final int Y_POSITION = 0;
    private static final int OFFSET = 0;

    /**
     *
     * @param context
     * @param isFrontCamera
     * @param data captured image byte array
     * @param imgOverlay
     * @return final image to be saved
     */
    public static Bitmap generateFinalImage(Context context, boolean isFrontCamera, byte[] data, Bitmap imgOverlay) {
        Bitmap cameraBitmap = getCorrectCapturedImageOrientation(context, isFrontCamera, data);
        int wid = cameraBitmap.getWidth();
        int hgt = cameraBitmap.getHeight();
        return drawOverlaidImageOnCapturedImage(imgOverlay, cameraBitmap, wid, hgt);
    }

    private static Bitmap getCorrectCapturedImageOrientation(Context context, boolean isFrontCamera, byte[] data) {
        Bitmap cameraBitmap = null;
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                cameraBitmap = getPortraitCapturedImageBitmap(isFrontCamera, data);

            } catch (OutOfMemoryError | Exception e) {
                e.printStackTrace();
            }
        } else {
            cameraBitmap = getLandscapeCapturedImageBitmap(isFrontCamera, data);
        }
        return cameraBitmap;
    }

    private static Bitmap getLandscapeCapturedImageBitmap(boolean isFrontCamera, byte[] data) {
        Bitmap cameraBitmap;
        if (isFrontCamera) {
            // The below to mirror the image back, front camera issue
            float[] mirrorY = {-1, 0, 0, 0, 1, 0, 0, 0, 1};
            Matrix matrix = new Matrix();
            Matrix matrixMirrorY = new Matrix();
            matrixMirrorY.setValues(mirrorY);

            matrix.postConcat(matrixMirrorY);

            Bitmap rotatedImg = BitmapFactory.decodeByteArray(data, OFFSET,
                    data.length);

            cameraBitmap = Bitmap.createBitmap(rotatedImg, X_POSITION, Y_POSITION,
                    rotatedImg.getWidth(), rotatedImg.getHeight(),
                    matrix, true);
        } else {
            cameraBitmap = BitmapFactory.decodeByteArray(data, OFFSET,
                    data.length);
        }
        return cameraBitmap;
    }

    private static Bitmap drawOverlaidImageOnCapturedImage(Bitmap imgOverlay, Bitmap cameraBitmap, int wid, int hgt) {
        //create new image with the same dimensions of the camera captured image
        Bitmap finalImage = Bitmap.createBitmap(wid, hgt,
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(finalImage);
        canvas.drawBitmap(cameraBitmap, LEFT_POSITION, TOP_POSITION, null);
        if (imgOverlay != null) {
            //scale overlay bitmap with the same dimensions of the camera captured image
            imgOverlay = Bitmap.createScaledBitmap(imgOverlay, wid, hgt, true);
            canvas.drawBitmap(imgOverlay, LEFT_POSITION, TOP_POSITION, null);
        }
        return finalImage;
    }

    private static Bitmap getPortraitCapturedImageBitmap(boolean isFrontCamera, byte[] data) {
        Bitmap cameraBitmap = BitmapFactory.decodeByteArray(data, OFFSET,
                data.length);

        int wid = cameraBitmap.getWidth();
        int hgt = cameraBitmap.getHeight();
        Matrix rotateRight;
        if (isFrontCamera) {
            //  for Front Camera to Rotate captured image
            rotateRight = new Matrix();
            rotateRight.preRotate(90);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR2) {
                // apply below matrix  to resolve image mirroring
                float[] mirrorY = {-1, 0, 0, 0, 1, 0, 0, 0, 1};
                rotateRight = new Matrix();
                Matrix matrixMirrorY = new Matrix();
                matrixMirrorY.setValues(mirrorY);

                rotateRight.postConcat(matrixMirrorY);

                rotateRight.preRotate(270);

            }
            // correct Bitmap
            cameraBitmap = Bitmap.createBitmap(cameraBitmap, X_POSITION, Y_POSITION, wid, hgt, rotateRight, true);
        } else {
            Matrix mtx = new Matrix();
            mtx.postRotate(90);
            // Rotating Bitmap
            cameraBitmap = Bitmap.createBitmap(cameraBitmap, X_POSITION, Y_POSITION, wid, hgt, mtx, true);
        }
        return cameraBitmap;
    }
}
