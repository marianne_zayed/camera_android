package com.linkdev.camera.helpers;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Environment;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.linkdev.camera.activities.ActivityNewCamera;
import com.linkdev.camera.activities.ActivityViewCapturedImage;

import java.io.File;

/**
 * Created by Marianne on 05-Feb-17.
 */

public class SaveImageAsyncTask extends AsyncTask<Void, Void, Void> {
    private RelativeLayout relProgressBar;
    private String filename;
    private Bitmap image;
    private Context context;

    public SaveImageAsyncTask(Context context, RelativeLayout relProgressBar, Bitmap image) {
        this.relProgressBar = relProgressBar;
        this.image = image;
        this.context = context;
    }


    @Override
    protected Void doInBackground(Void... voids) {
        if (isCancelled())
            return null;
        else {
            File storagePath = new File(Environment.getExternalStorageDirectory()
                    + Constants.appFolder + Constants.selfieFolder);
            storagePath.mkdirs();
            filename = Long.toString(System.currentTimeMillis()) + ".jpg";
            File file = new File(storagePath, filename);
            filename = storagePath + "/" + filename;
            Utilities.saveFile(file, image);
            Utilities.refreshGallery(file, context);
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        relProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        relProgressBar.setVisibility(View.GONE);
        ActivityViewCapturedImage.startActivity(context, filename);
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
    }
}
