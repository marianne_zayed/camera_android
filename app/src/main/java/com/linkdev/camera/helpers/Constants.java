package com.linkdev.camera.helpers;

/**
 * Created by Marianne.Wazif on 11-Jun-17.
 */

public class Constants {
    public static final String appFolder = "/Camera App";
    public static final String selfieFolder = "/Selfie/";
    public static final String IMAGE_URL = "image_url";
    public static final String ARG_HAS_IMAGE_OVERLAID = "hasImageOverlaid";
    public static final String ARG_IS_FRONT_CAMERA = "isFrontCamera";
    public static final String ARG_IMAGE_URL = "imageUrl";
    public static final String ARG_DRAWABLE_RESOURCE_ID = "resId";
    public static final Boolean DEFAULT_CAMERA = false;
    public static final String OLD_CAMERA = "old camera";
    public static final String NEW_CAMERA = "new camera";

}
