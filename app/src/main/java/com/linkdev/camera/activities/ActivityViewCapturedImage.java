package com.linkdev.camera.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;


import com.linkdev.camera.R;
import com.linkdev.camera.helpers.Constants;

import java.io.File;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class ActivityViewCapturedImage extends AppCompatActivity {
    private String imagePath;
    private Button btnShare, btnCancel;
    private ImageView imgCaptured;

    public static void startActivity(Context context, String imagePath) {
        Intent i = new Intent(context, ActivityViewCapturedImage.class);
        i.putExtra(Constants.IMAGE_URL, imagePath);
        i.addFlags(FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_view_captured_image);
        if (getIntent() != null) {
            imagePath = getIntent().getStringExtra(Constants.IMAGE_URL);
        }
        initViews();
        setListeners();
        openCapturedImage();
    }

    private void openCapturedImage() {
        File imgFile = new File(imagePath);

        if (imgFile.exists()) {

            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());

            imgCaptured.setImageBitmap(myBitmap);
        } else {
            Toast.makeText(this, "Image is not existed", Toast.LENGTH_SHORT).show();
        }
    }

    private void initViews() {
        btnShare = (Button) findViewById(R.id.btnShare);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        imgCaptured = ((ImageView) findViewById(R.id.imgCaptured));
    }

    private void setListeners() {
        btnShare.setOnClickListener(shareButtonClickListener);
        btnCancel.setOnClickListener(closeButtonClickListener);
    }

    private View.OnClickListener shareButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            shareImage();
        }
    };

    private View.OnClickListener closeButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private void shareImage() {
        Intent share = new Intent(Intent.ACTION_SEND);

        // If you want to share a png image only, you can do:
        // setType("image/png"); OR for jpeg: setType("image/jpeg");
        share.setType("image/*");

        // Make sure you put example png image named myImage.png in your
        // directory
        if (imagePath.length() < 1) return;

        File imageFileToShare = new File(imagePath);

        Uri uri = Uri.fromFile(imageFileToShare);
        share.putExtra(Intent.EXTRA_STREAM, uri);
        startActivity(Intent.createChooser(share, "Share Image!"));
    }
}
