package com.linkdev.camera.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.linkdev.camera.R;
import com.linkdev.camera.helpers.CameraManagerHelper;
import com.linkdev.camera.helpers.Constants;
import com.linkdev.camera.helpers.SaveImageAsyncTask;
import com.linkdev.camera.views.AutoFitTextureView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * References : https://willowtreeapps.com/ideas/camera2-and-you-leveraging-android-lollipops-new-camera/
 * https://inducesmile.com/android/android-camera2-api-example-tutorial/
 * https://developer.android.com/samples/Camera2Basic/src/com.example.android.camera2basic/Camera2BasicFragment.html#l491
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class ActivityNewCamera extends AppCompatActivity {
    private static final String TAG = ActivityNewCamera.class.getSimpleName();
    private Button btnCapture, btnCancel;
    private AutoFitTextureView textureViewCamera;
    private ImageView imgCameraSwitcher;
    private ImageView imgOverlaid;
    private Bitmap finalImage;
    /**
     * Camera state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * Camera state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * Camera state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * Camera state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * Camera state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;
    private int state = STATE_PREVIEW;
    private final int QUALITY = 100;
    private int mSensorOrientation;
    private RelativeLayout relProgressBar;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest captureRequest;
    private CaptureRequest.Builder captureRequestBuilder;
    private ImageReader imageReader;
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    private final int FRONT_CAMERA_ID = 1;
    private final int BACK_CAMERA_ID = 0;
    private boolean hasImageOverlaid;
    private boolean isFrontCamera;
    private int previewWidth;
    private int previewHeight;
    private String imgUrl;
    private int drawableResID;
    private Size previewSize;
    private SaveImageAsyncTask saveImageAsyncTask;
    /**
     * Max preview width that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    public static void startActivity(Context context, boolean hasImageOverlaid, boolean isFrontCamera, String imgUrl) {
        Intent i = new Intent(context, ActivityNewCamera.class);
        i.putExtra(Constants.ARG_HAS_IMAGE_OVERLAID, hasImageOverlaid);
        i.putExtra(Constants.ARG_IS_FRONT_CAMERA, isFrontCamera);
        i.putExtra(Constants.ARG_IMAGE_URL, imgUrl);
        context.startActivity(i);
    }

    public static void startActivity(Context context, boolean hasImageOverlaid, boolean isFrontCamera, int drawableResID) {
        Intent i = new Intent(context, ActivityNewCamera.class);
        i.putExtra(Constants.ARG_HAS_IMAGE_OVERLAID, hasImageOverlaid);
        i.putExtra(Constants.ARG_IS_FRONT_CAMERA, isFrontCamera);
        i.putExtra(Constants.ARG_DRAWABLE_RESOURCE_ID, drawableResID);
        context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_new_camera);
        if (getIntent() != null) {
            hasImageOverlaid = getIntent().getBooleanExtra(Constants.ARG_HAS_IMAGE_OVERLAID, false);
            isFrontCamera = getIntent().getBooleanExtra(Constants.ARG_IS_FRONT_CAMERA, Constants.DEFAULT_CAMERA);
            imgUrl = getIntent().getStringExtra(Constants.ARG_IMAGE_URL);
            drawableResID = getIntent().getIntExtra(Constants.ARG_DRAWABLE_RESOURCE_ID, -1);
        }
        initViews();
        setListeners();
    }

    private void setListeners() {
        textureViewCamera.setSurfaceTextureListener(textureViewCameraListener);
        btnCapture.setOnClickListener(btnCaptureOnClickListener);
        btnCancel.setOnClickListener(closeButtonClickListener);
        imgCameraSwitcher.setOnClickListener(imgCameraSwitcherClickListener);
    }

    private View.OnClickListener closeButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
    private View.OnClickListener imgCameraSwitcherClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
            if (!TextUtils.isEmpty(imgUrl))
                ActivityNewCamera.startActivity(ActivityNewCamera.this, hasImageOverlaid, !isFrontCamera, imgUrl);
            else
                ActivityNewCamera.startActivity(ActivityNewCamera.this, hasImageOverlaid, !isFrontCamera, drawableResID);
        }
    };
    private View.OnClickListener btnCaptureOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            enableControls(false);
            takePicture();
        }
    };

    private void enableControls(boolean enable) {
        imgCameraSwitcher.setEnabled(enable);
        btnCancel.setEnabled(enable);
        btnCapture.setEnabled(enable);
    }

    private void initViews() {
        textureViewCamera = (AutoFitTextureView) findViewById(R.id.textureViewCamera);
        btnCapture = (Button) findViewById(R.id.btnCapture);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        imgCameraSwitcher = (ImageView) findViewById(R.id.imgCameraSwitcher);
        imgOverlaid = (ImageView) findViewById(R.id.imgOverlaid);
        relProgressBar = (RelativeLayout) findViewById(R.id.relProgressBar);
    }

    private void setUpCameraOutputs(int width, int height) {
        try {
            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            Log.e(TAG, "is camera open");
            try {
                if (isFrontCamera)
                    cameraId = getFrontFacingCameraId(manager);
                else
                    cameraId = getBackFacingCameraId(manager);
                if (cameraId != null) {
                    CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                    //CameraCharacteristics object contains everything you could hope to know about the camera’s lens, sensor, and supported controls;
                    // you just have to access the specific values using the get() method.
                    // A key component that you’ll need is the StreamConfigurationMap,
                    // which contains the image formats and sizes that are supported by the camera
                    StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                    if (map == null)
                        return;
                    // For still image captures, we use the largest available size.
                    Size largest = Collections.max(
                            Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                            new CompareSizesByArea());
                    imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(),
                            ImageFormat.JPEG, /*maxImages*/2);
                    imageReader.setOnImageAvailableListener(
                            readerListener, null);

                    // Find out if we need to swap dimension to get the preview size relative to sensor
                    // coordinate.
                    int displayRotation = getWindowManager().getDefaultDisplay().getRotation();
                    //noinspection ConstantConditions
                    mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                    boolean swappedDimensions = false;
                    switch (displayRotation) {
                        case Surface.ROTATION_0:
                        case Surface.ROTATION_180:
                            if (mSensorOrientation == 90 || mSensorOrientation == 270) {
                                swappedDimensions = true;
                            }
                            break;
                        case Surface.ROTATION_90:
                        case Surface.ROTATION_270:
                            if (mSensorOrientation == 0 || mSensorOrientation == 180) {
                                swappedDimensions = true;
                            }
                            break;
                        default:
                            Log.e(TAG, "Display rotation is invalid: " + displayRotation);
                    }

                    Point displaySize = new Point();
                    getWindowManager().getDefaultDisplay().getSize(displaySize);
                    int rotatedPreviewWidth = width;
                    int rotatedPreviewHeight = height;
                    int maxPreviewWidth = displaySize.x;
                    int maxPreviewHeight = displaySize.y;

                    if (swappedDimensions) {
                        rotatedPreviewWidth = height;
                        rotatedPreviewHeight = width;
                        maxPreviewWidth = displaySize.y;
                        maxPreviewHeight = displaySize.x;
                    }

                    if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                        maxPreviewWidth = MAX_PREVIEW_WIDTH;
                    }

                    if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                        maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                    }

                    // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                    // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                    // garbage capture data.
                    previewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
                            rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
                            maxPreviewHeight, largest);

                    // We fit the aspect ratio of TextureView to the size of preview we picked.
                    int orientation = getResources().getConfiguration().orientation;
                    if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                        textureViewCamera.setAspectRatio(
                                previewSize.getWidth(), previewSize.getHeight());
                        previewWidth = previewSize.getWidth();
                        previewHeight = previewSize.getHeight();
                    } else {
                        textureViewCamera.setAspectRatio(
                                previewSize.getHeight(), previewSize.getWidth());
                        previewWidth = previewSize.getHeight();
                        previewHeight = previewSize.getWidth();
                    }

                }
            } catch (CameraAccessException | NullPointerException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link android.graphics.Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        if (null == textureViewCamera || null == previewSize) {
            return;
        }
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / previewSize.getHeight(),
                    (float) viewWidth / previewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        textureViewCamera.setTransform(matrix);
    }


    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */

    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                          int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                } else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    private static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }

    /**
     * load overlaid image using Picasso if any
     */
    private void loadImageOverlay() {
        if (!TextUtils.isEmpty(imgUrl)) {
            relProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(imgUrl)
                    .fit()
                    .into(imgOverlaid, picassoListenerCallBack);
        } else
            imgOverlaid.setImageDrawable(ContextCompat.getDrawable(this, drawableResID));
    }

    private Callback picassoListenerCallBack = new Callback() {
        @Override
        public void onSuccess() {
            relProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError() {
            relProgressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), getString(R.string.can_not_load_img), Toast.LENGTH_SHORT).show();
            finish();
        }
    };
    private TextureView.SurfaceTextureListener textureViewCameraListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
            //open your camera here
            openCamera(width, height);
            if (hasImageOverlaid) {
                initImageOverlayDimensions();
                loadImageOverlay();
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
            // Transform you image captured size according to the surface width and height
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {
        }
    };

    private void initImageOverlayDimensions() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(previewWidth, previewHeight);
        imgOverlaid.setLayoutParams(layoutParams);
    }


    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            //This is called when the camera is open
            Log.e(TAG, "onOpened");
            cameraDevice = camera;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            cameraDevice.close();
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            cameraDevice.close();
            cameraDevice = null;
        }
    };
    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (state) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        capturePicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            state = STATE_PICTURE_TAKEN;
                            capturePicture();
                        } else {
                            runPreCaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        state = STATE_WAITING_NON_PRECAPTURE;
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        state = STATE_PICTURE_TAKEN;
                        capturePicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {

        // This is how to tell the camera to lock focus.
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                CameraMetadata.CONTROL_AF_TRIGGER_START);
        // Tell #mCaptureCallback to wait for the lock.
        state = STATE_WAITING_LOCK;
        capturePicture();
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPreCaptureSequence() {
        try {
            // This is how to tell the camera to trigger.
            captureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            state = STATE_WAITING_PRECAPTURE;
            cameraCaptureSessions.capture(captureRequestBuilder.build(), mCaptureCallback,
                    null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void capturePicture() {
        try {
            if (null == cameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(imageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);

            cameraCaptureSessions.stopRepeating();
            cameraCaptureSessions.capture(captureBuilder.build(), mCaptureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    protected void takePicture() {
        lockFocus();
    }

    private ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            Image image = null;
            try {
                Bitmap imgOverlay = null;
                if (hasImageOverlaid)
                    imgOverlay = getImageOverlayBitmap();
                image = reader.acquireLatestImage();
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] capturedImageBytes = new byte[buffer.capacity()];
                buffer.get(capturedImageBytes);
                finalImage = CameraManagerHelper.generateFinalImage(ActivityNewCamera.this, isFrontCamera, capturedImageBytes, imgOverlay);
                save();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (image != null) {
                    image.close();
                }
            }
        }
    };

    private Bitmap getImageOverlayBitmap() {
        imgOverlaid.buildDrawingCache();
        return imgOverlaid.getDrawingCache();
    }

    /**
     * save the newly created image in "app name" Folder in subfolder "selfie"
     */
    private void save() {
        saveImageAsyncTask = new SaveImageAsyncTask(this, relProgressBar, finalImage);
        saveImageAsyncTask.execute();
    }

    private String getFrontFacingCameraId(CameraManager cameraManager) {
        try {
            return cameraManager.getCameraIdList()[FRONT_CAMERA_ID];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    private String getBackFacingCameraId(CameraManager cameraManager) {
        try {
            return cameraManager.getCameraIdList()[BACK_CAMERA_ID];
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = textureViewCamera.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);
            // We set up a CaptureRequest.Builder with the output Surface.
            captureRequestBuilder
                    = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(surface);

            // Here, we create a CameraCaptureSession for camera preview.
            cameraDevice.createCaptureSession(Arrays.asList(surface, imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            // The camera is already closed
                            if (null == cameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            cameraCaptureSessions = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                captureRequest = captureRequestBuilder.build();
                                cameraCaptureSessions.setRepeatingRequest(captureRequest,
                                        mCaptureCallback, null);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void openCamera(int width, int height) {
        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            manager.openCamera(cameraId, stateCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if (null != cameraDevice) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (null != imageReader) {
            imageReader.close();
            imageReader = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                // close the app
                Toast.makeText(ActivityNewCamera.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
        enableControls(true);
        if (textureViewCamera.isAvailable()) {
            openCamera(textureViewCamera.getWidth(), textureViewCamera.getHeight());
        } else {
            textureViewCamera.setSurfaceTextureListener(textureViewCameraListener);
        }
    }

    @Override
    protected void onPause() {
        Log.e(TAG, "onPause");
        closeCamera();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (saveImageAsyncTask != null)
            saveImageAsyncTask.cancel(true);
    }
}
