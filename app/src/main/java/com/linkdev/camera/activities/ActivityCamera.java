package com.linkdev.camera.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.linkdev.camera.R;
import com.linkdev.camera.helpers.CameraManagerHelper;
import com.linkdev.camera.helpers.Constants;
import com.linkdev.camera.helpers.SaveImageAsyncTask;
import com.linkdev.camera.helpers.Utilities;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;

/**
 * Note: check on Camera and Write to Storage Permissions Before Starting this Activity for API 23 and Above
 */
public class ActivityCamera extends AppCompatActivity implements SurfaceHolder.Callback {
    private Button btnCapture, btnCancel;
    private ImageView imgCameraSwitcher;
    private Camera camera;
    private ImageView imgOverlaid;
    private SurfaceView surfaceViewCamera;
    private Bitmap finalImage;
    private int screenWidth;
    private int screenHeight;
    private FrameLayout frmMainLayout;
    private String imgUrl;
    private int previewWidth = 0;
    private int previewHeight = 0;
    private SurfaceHolder mHolder;
    private float aspectRatioPreview;
    private boolean hasImageOverlaid;
    private final String TAG = ActivityCamera.class.getName();
    private final int ROUNDING_FACTOR = 2;
    private final double ASPECT_TOLERANCE_RATIO = 0.05;
    private boolean isFrontCamera;
    private int drawableResID;
    private SaveImageAsyncTask saveImageAsyncTask;
    private RelativeLayout relProgressBar;

    public static void startActivity(Context context, boolean hasImageOverlaid, boolean isFrontCamera, String imgUrl) {
        Intent i = new Intent(context, ActivityCamera.class);
        i.putExtra(Constants.ARG_HAS_IMAGE_OVERLAID, hasImageOverlaid);
        i.putExtra(Constants.ARG_IS_FRONT_CAMERA, isFrontCamera);
        i.putExtra(Constants.ARG_IMAGE_URL, imgUrl);
        context.startActivity(i);
    }

    public static void startActivity(Context context, boolean hasImageOverlaid, boolean isFrontCamera, int drawableResID) {
        Intent i = new Intent(context, ActivityCamera.class);
        i.putExtra(Constants.ARG_HAS_IMAGE_OVERLAID, hasImageOverlaid);
        i.putExtra(Constants.ARG_IS_FRONT_CAMERA, isFrontCamera);
        i.putExtra(Constants.ARG_DRAWABLE_RESOURCE_ID, drawableResID);
        context.startActivity(i);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);
        if (getIntent() != null) {
            hasImageOverlaid = getIntent().getBooleanExtra(Constants.ARG_HAS_IMAGE_OVERLAID, false);
            isFrontCamera = getIntent().getBooleanExtra(Constants.ARG_IS_FRONT_CAMERA, Constants.DEFAULT_CAMERA);
            imgUrl = getIntent().getStringExtra(Constants.ARG_IMAGE_URL);
            drawableResID = getIntent().getIntExtra(Constants.ARG_DRAWABLE_RESOURCE_ID, -1);
        }
        if (isFrontCamera && !Utilities.checkFrontCameraHardware(this)) {
            Toast.makeText(this, getString(R.string.no_front_camera), Toast.LENGTH_SHORT).show();
            finish();
        }
        initViews();
        setListeners();
        initSurfaceHolder();
    }

    private void enableControls(boolean enable) {
        imgCameraSwitcher.setEnabled(enable);
        btnCancel.setEnabled(enable);
        btnCapture.setEnabled(enable);
    }

    private void initViews() {
        surfaceViewCamera = (SurfaceView) findViewById(R.id.surfaceViewCamera);
        imgOverlaid = (ImageView) findViewById(R.id.imgOverlaid);
        frmMainLayout = (FrameLayout) findViewById(R.id.frmMainLayout);
        btnCapture = (Button) findViewById(R.id.btnCapture);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        imgCameraSwitcher = (ImageView) findViewById(R.id.imgCameraSwitcher);
        relProgressBar = (RelativeLayout) findViewById(R.id.relProgressBar);
    }


    /**
     * load overlaid image using Picasso if any
     */
    private void loadImageOverlay() {
        if (!TextUtils.isEmpty(imgUrl)) {
            relProgressBar.setVisibility(View.VISIBLE);
            Picasso.with(this)
                    .load(imgUrl)
                    .fit()
                    .into(imgOverlaid, picassoListenerCallBack);
        } else
            imgOverlaid.setImageDrawable(ContextCompat.getDrawable(this, drawableResID));
    }

    private Callback picassoListenerCallBack = new Callback() {
        @Override
        public void onSuccess() {
            relProgressBar.setVisibility(View.GONE);
        }

        @Override
        public void onError() {
            relProgressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), getString(R.string.can_not_load_img), Toast.LENGTH_SHORT).show();
            finish();
        }
    };

    private void setListeners() {
        btnCapture.setOnClickListener(captureButtonClickListener);
        btnCancel.setOnClickListener(closeButtonClickListener);
        imgCameraSwitcher.setOnClickListener(imgCameraSwitcherClickListener);
    }

    private View.OnClickListener captureButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (camera != null) {
                capture();
            }
        }
    };
    private View.OnClickListener imgCameraSwitcherClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
            if (!TextUtils.isEmpty(imgUrl))
                ActivityCamera.startActivity(ActivityCamera.this, hasImageOverlaid, !isFrontCamera, imgUrl);
            else
                ActivityCamera.startActivity(ActivityCamera.this, hasImageOverlaid, !isFrontCamera, drawableResID);
        }
    };

    private View.OnClickListener closeButtonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };

    private void capture() {
        enableControls(false);
        camera.takePicture(null, null,
                picTakenCallback);
    }

    /**
     * calculate the surface view Correct Dimensions  relative to Camera preview dimensions
     */
    private void initSurfacePreviewDimensions() {
        Camera.Parameters params = camera.getParameters();
        Camera.Size sizeg = params.getPreviewSize();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        setCorrectPreviewWidthHeight(sizeg, size);
        surfaceViewCamera.setLayoutParams(new FrameLayout.LayoutParams(previewWidth, previewHeight));
    }

    private void setCorrectPreviewWidthHeight(Camera.Size sizeg, Point size) {
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            try {
                screenHeight = size.y;
                screenWidth = size.x;
                aspectRatioPreview = (float) sizeg.width
                        / (float) sizeg.height;
                //Note: camera preview size width is always greater than height in portrait mode
                //set the correct preview width and Height
                if (sizeg.height < screenWidth || sizeg.height > screenWidth) {
                    previewHeight = (int) (screenWidth * aspectRatioPreview);
                    previewWidth = screenWidth;
                }// swap them
                else {
                    previewWidth = sizeg.height;
                    previewHeight = sizeg.width;
                }


            } catch (OutOfMemoryError | Exception ex) {
                ex.printStackTrace();
            }
        } else {
            int screenHeight = size.y;
            previewWidth = 0;
            previewHeight = 0;
            if (sizeg.height < screenHeight) {
                aspectRatioPreview = (float) sizeg.width
                        / (float) sizeg.height;

                previewWidth = (int) (screenHeight * aspectRatioPreview);
                previewHeight = screenHeight;

            } else if (sizeg.height > screenHeight) {
                float aspectRatioPreview = (float) sizeg.width
                        / (float) sizeg.height;

                previewWidth = (int) (screenHeight * aspectRatioPreview);
                previewHeight = screenHeight;

            } else {
                previewWidth = sizeg.width;
                previewHeight = sizeg.height;
            }
            screenWidth = previewWidth;
            screenHeight = previewHeight;
        }
    }

    /**
     * set image View width and height to match calculated camera surface Preview dimensions
     */
    private void initImageOverlayDimensions() {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(previewWidth, previewHeight);
        imgOverlaid.setLayoutParams(layoutParams);
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableControls(true);
        if (camera != null) {
            camera.startPreview();
        }
    }


    private void OpenCamera() {
        int cameraId = -1;
        int numberOfCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numberOfCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT && isFrontCamera) {
                cameraId = i;
                break;
            } else if (info.facing == Camera.CameraInfo.CAMERA_FACING_BACK && !isFrontCamera) {
                cameraId = i;
                break;
            }
        }

        camera = Camera.open(cameraId);
        setCameraDisplayOrientation(cameraId);
        //Rotation is to rotate the dimensions for the captured image
        if (isFrontCamera)
            camera.getParameters().setRotation(90);
    }

    /**
     * set Camera correct Orientation for portrait
     *
     * @param cameraId
     */
    private void setCameraDisplayOrientation(int cameraId) {

        Camera.CameraInfo info = new Camera.CameraInfo();

        Camera.getCameraInfo(cameraId, info);

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
            Log.e("result ", result + "");

        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }
        camera.setDisplayOrientation(result);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ReleaseCamera();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void ReleaseCamera() {
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
    }


    private Camera.PictureCallback picTakenCallback = new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {
            try {
                camera.stopPreview();
                Bitmap imgOverlay = null;
                if (hasImageOverlaid)
                    imgOverlay = getImageOverlayBitmap();
                finalImage = CameraManagerHelper.generateFinalImage(ActivityCamera.this, isFrontCamera, data, imgOverlay);
                save();
            } catch (OutOfMemoryError | Exception outOfMemory) {
                outOfMemory.printStackTrace();
            }
        }
    };

    private Bitmap getImageOverlayBitmap() {
        imgOverlaid.buildDrawingCache();
        return imgOverlaid.getDrawingCache();
    }


    /**
     * save the newly created image in "app name" Folder in subfolder "selfie"
     */
    private void save() {
        saveImageAsyncTask = new SaveImageAsyncTask(this, relProgressBar, finalImage);
        saveImageAsyncTask.execute();
    }

    /**
     * set listener to surface view holder to open camera on it
     */
    public void initSurfaceHolder() {
        mHolder = surfaceViewCamera.getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        // The Surface has been created, open camera here and load image overlay
        OpenCamera();
        if (hasImageOverlaid)
            loadImageOverlay();
        try {
            if (camera != null) {
                camera.setPreviewDisplay(surfaceHolder);
                Camera.Parameters params = camera.getParameters();
                List<String> focusModes = params.getSupportedFocusModes();
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                    // set the focus mode
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                    // set Camera parameters
                    camera.setParameters(params);
                }
            }
        } catch (IOException exception) {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        if (camera != null) {

            initSurfacePreviewDimensions();
            if (hasImageOverlaid)
                initImageOverlayDimensions();
            Camera.Parameters parameters = getBestCameraParameters();
            camera.setParameters(parameters);
            camera.startPreview();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        // Surface will be destroyed when we return, so release camera.
        if (camera != null) {
            ReleaseCamera();
        }
    }

    /**
     * calculate the Best camera Preview size and Taken Picture size
     *
     * @return Best parameters to be set for camera
     */
    @NonNull
    private Camera.Parameters getBestCameraParameters() {
        //set optimal Camera preview size based on calculated preview width and height
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size bestPreviewSize = getOptimalPreviewSize(parameters.getSupportedPreviewSizes(), previewWidth, previewHeight);
        parameters.setPreviewSize(bestPreviewSize.width, bestPreviewSize.height);
        aspectRatioPreview = (float) bestPreviewSize.width / (float) bestPreviewSize.height;
        //set optimal captured picture size based on the aspect ratio of the best preview width and size
        for (int j = 0; j < parameters.getSupportedPictureSizes().size(); j++) {
            Camera.Size pictureSize = parameters.getSupportedPictureSizes().get(j);
            float pictureSizeAspectRatio = Utilities.round((float) pictureSize.width / (float) pictureSize.height, ROUNDING_FACTOR);
            aspectRatioPreview = Utilities.round(aspectRatioPreview, ROUNDING_FACTOR);
            if (pictureSizeAspectRatio == aspectRatioPreview) {
                parameters.setPictureSize(pictureSize.width, pictureSize.height);
                break;
            }
        }
        return parameters;
    }


    /**
     * to get the optimal preview width and height for the camera based on the surface view width and height
     *
     * @param sizes supported size of camera
     * @param w     preview width
     * @param h     preview height
     * @return the best camera preview width and height
     */
    private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
        Camera.Size optimalSize = null;
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            //Note that size.height is equivalent to preview width and same for size.width
            final double ASPECT_TOLERANCE = ASPECT_TOLERANCE_RATIO;
            double targetRatio = (double) w / h;
            if (sizes == null) return null;

            double minDiff = Double.MAX_VALUE;

            int targetWidth = w;

            // Try to find a size match aspect ratio and size
            for (Camera.Size size : sizes) {
                double ratio = (double) size.height / size.width;
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
                if (Math.abs(size.height - targetWidth) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetWidth);
                }
            }

            // Cannot find the one match the aspect ratio, ignore the requirement
            if (optimalSize == null) {
                minDiff = Double.MAX_VALUE;
                for (Camera.Size size : sizes) {
                    if (Math.abs(size.height - targetWidth) < minDiff) {
                        optimalSize = size;
                        minDiff = Math.abs(size.height - targetWidth);
                    }
                }
            }
        } else {
            final double ASPECT_TOLERANCE = 0.1;
            double targetRatio = (double) w / h;
            if (sizes == null) return null;


            double minDiff = Double.MAX_VALUE;

            int targetHeight = h;

            // Try to find an size match aspect ratio and size
            for (Camera.Size size : sizes) {
                double ratio = (double) size.width / size.height;
                if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
                if (Math.abs(size.height - targetHeight) < minDiff) {
                    optimalSize = size;
                    minDiff = Math.abs(size.height - targetHeight);
                }
            }

            // Cannot find the one match the aspect ratio, ignore the requirement
            if (optimalSize == null) {
                minDiff = Double.MAX_VALUE;
                for (Camera.Size size : sizes) {
                    if (Math.abs(size.height - targetHeight) < minDiff) {
                        optimalSize = size;
                        minDiff = Math.abs(size.height - targetHeight);
                    }
                }
            }
        }
        return optimalSize;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (saveImageAsyncTask != null)
            saveImageAsyncTask.cancel(true);
    }
}
