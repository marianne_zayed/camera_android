package com.linkdev.camera.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.linkdev.camera.R;
import com.linkdev.camera.helpers.Constants;
import com.linkdev.camera.interfaces.PermissionsInterfaceListener;

public class ActivityMain extends ActivityPermissionsHandler implements PermissionsInterfaceListener {
    private Button btnCamera, btnNewCamera;
    private CheckBox chkCameraOverlaid, chkNewCameraOverlaid;
    private String cameraType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        setListeners();
    }

    private void setListeners() {
        btnCamera.setOnClickListener(btnCameraPortraitOnClickListener);
        btnNewCamera.setOnClickListener(btnNewCameraOnClickListener);
    }

    private void initViews() {
        btnCamera = (Button) findViewById(R.id.btnCamera);
        btnNewCamera = (Button) findViewById(R.id.btnNewCamera);
        chkCameraOverlaid = (CheckBox) findViewById(R.id.chkCameraOverlaid);
        chkNewCameraOverlaid = (CheckBox) findViewById(R.id.chkNewCameraOverlaid);
    }

    private View.OnClickListener btnCameraPortraitOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            cameraType = Constants.OLD_CAMERA;
            checkCameraAndWriteToStoragePermissions();
        }
    };
    private View.OnClickListener btnNewCameraOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            cameraType = Constants.NEW_CAMERA;
            checkCameraAndWriteToStoragePermissions();
        }
    };

    private void openActivity() {
        switch (cameraType) {
            //for url use this link: http://etaweb1.cloudapp.net:81//api/Images/?imageId=00a9bf34-fe86-4766-84f1-483841575700
            case Constants.OLD_CAMERA: {
                ActivityCamera.startActivity(ActivityMain.this, chkCameraOverlaid.isChecked(), Constants.DEFAULT_CAMERA, R.drawable.image_overlaid_portrait);
                break;
            }
            case Constants.NEW_CAMERA: {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    ActivityNewCamera.startActivity(ActivityMain.this, chkNewCameraOverlaid.isChecked(), Constants.DEFAULT_CAMERA, R.drawable.image_overlaid_portrait);
                } else
                    Toast.makeText(ActivityMain.this, "New Camera is not supported on this Android Version", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }

    @Override
    public void onWriteExternalStorageGranted() {

    }

    @Override
    public void onWriteExternalStorageDenied() {

    }

    @Override
    public void onCameraAccessGranted() {

    }

    @Override
    public void onCameraAccessDenied() {

    }

    @Override
    public void onCameraAccessAndWriteExternalStorageDenied() {

    }

    @Override
    public void onCameraAccessAndWriteExternalStorageGranted() {
        openActivity();
    }
}
